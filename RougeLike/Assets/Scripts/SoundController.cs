﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

    public static SoundController Instance;
    public AudioSource soundEffect;


	void Awake () {
	    if(Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
	}

    public void PlaySingle(params AudioClip[] clips)
    {
        RandomizeSoundEffet(clips);
        soundEffect.Play();
    }

   private void RandomizeSoundEffet(AudioClip[] clips)
    {
        int randomSoundIndex = Random.Range(0, clips.Length);
        soundEffect.clip = clips[randomSoundIndex];
    }
	
}
