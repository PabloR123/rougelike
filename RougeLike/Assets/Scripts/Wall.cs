﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

    public int hitPoint = 2;
    public Sprite damagedWallSprites;

    private SpriteRenderer spriteRenderer;

	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
    public void DamageWall(int damageRecevied)
    {
        hitPoint -= damageRecevied;
        spriteRenderer.sprite = damagedWallSprites;

        if(hitPoint <=0)
        {
            gameObject.SetActive(false);
        }
    }

}
